import React, { useEffect, useState } from "react";

// IMPORT: components
import Navbar from "./components/Navbar/Navbar";
import ProductList from "./components/ProductList/ProductList";
import Cart from "./components/Cart/Cart";

// IMPORT: react-toastify
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const url = "https://fakestoreapi.com/products";

export const App = () => {
  const [isHomeComponent, setIsHomeComponent] = useState(true);
  const [isCartComponent, setIsCartComponent] = useState(false);
  const [cartItems, setCartItems] = useState([]);
  const [cartTotalProducts, setCartTotalProducts] = useState(0);
  const [cartTotalPrice, setCartTotalPrice] = useState(0);

  const [products, setProducts] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const getProducts = async () => {
    try {
      const response = await fetch(url);
      const products = await response.json();
      setProducts(products);
      setIsLoading(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);

  function onHomePress() {
    setIsHomeComponent(true);
    setIsCartComponent(false);
  }

  function onCartPress() {
    setIsHomeComponent(false);
    setIsCartComponent(true);
  }

  function addCartItem(item) {
    setCartItems((prevCartItems) => [...prevCartItems, item]);
    setCartTotalProducts(cartItems.length + 1);
    setCartTotalPrice(cartTotalPrice + item.price);
    toast.success(`Successfully added in the cart "${item.title}"`);
  }

  function removeItem(item, finalPrice) {
    setCartItems(cartItems.filter((filterdItem) => filterdItem.id !== item.id));
    setCartTotalProducts(cartItems.length - 1);
    setCartTotalPrice(cartTotalPrice - finalPrice);
    toast.warning("Successfully removed item");
  }

  function removeItemFromHome(item) {
    setCartItems(cartItems.filter((filterdItem) => filterdItem.id !== item.id));
    setCartTotalProducts(cartItems.length - 1);
    setCartTotalPrice(cartTotalPrice - item.price);
    toast.warning("Successfully removed item");
  }

  function clearCart() {
    setCartItems([]);
    setCartTotalProducts(0);
    setIsHomeComponent(true);
    setIsCartComponent(false);
    setCartTotalPrice(0);
    toast.warning("Your cart is clear now!");
  }

  function buyNow() {
    setCartItems([]);
    setCartTotalProducts(0);
    setIsHomeComponent(true);
    setIsCartComponent(false);
    setCartTotalPrice(0);
    toast.success("Thanks for purchasing");
  }

  // function cartTotal() {
  //   let newCartItems = cartItems.reduce((acc, curVal) => {
  //     return acc + curVal.price;
  //   }, 0);
  //   setCartTotalPrice(newCartItems);
  // }

  // useEffect(() => {
  //   cartTotal();
  // });

  return (
    <>
      <Navbar
        isHomeComponent={isHomeComponent}
        onHomePress={onHomePress}
        onCartPress={onCartPress}
        cartTotalProducts={cartTotalProducts}
      />
      {isHomeComponent && (
        <ProductList
          addCartItem={addCartItem}
          removeItem={removeItem}
          cartItems={cartItems}
          products={products}
          isLoading={isLoading}
          removeItemFromHome={removeItemFromHome}
        />
      )}
      {isCartComponent && (
        <Cart
          cartItems={cartItems}
          removeItem={removeItem}
          clearCart={clearCart}
          buyNow={buyNow}
          cartTotalPrice={cartTotalPrice}
          setCartTotalPrice={setCartTotalPrice}
          // finalCartTotalPrice={finalCartTotalPrice}
          // setFinalCartTotalPrice={setFinalCartTotalPrice}
        />
      )}
      <ToastContainer position="bottom-right" />
    </>
  );
};
