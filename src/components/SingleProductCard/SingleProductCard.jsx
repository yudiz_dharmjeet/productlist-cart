import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

import "./SingleProductCard.css";

const SingleProductCard = ({
  singleProduct,
  addCartItem,
  cartItems,
  removeItemFromHome,
}) => {
  const { title, image, description, price } = singleProduct;

  const [addCartButton, setAddCartButton] = useState(true);

  let product = cartItems.includes(singleProduct);

  useEffect(() => {
    if (product) {
      setAddCartButton(false);
    } else {
      setAddCartButton(true);
    }
  });

  return (
    <>
      <div className="card">
        <img src={image} alt={title} />
        <div className="card__data">
          <h4>{title}</h4>
          <p>{description.slice(0, 90) + "..."}</p>
          <div className="card__addtocart">
            <h4>${price}</h4>
            {addCartButton ? (
              <button
                className="btn"
                onClick={() => addCartItem(singleProduct)}
              >
                Add to cart
              </button>
            ) : (
              <button
                className="btn"
                onClick={() => removeItemFromHome(singleProduct)}
              >
                Remove from cart
              </button>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

SingleProductCard.propTypes = {
  singleProduct: PropTypes.shape({
    title: PropTypes.string,
    image: PropTypes.string,
    description: PropTypes.string,
    price: PropTypes.number,
  }),
  addCartItem: PropTypes.func,
  cartItems: PropTypes.array,
  removeItemFromHome: PropTypes.func,
};

export default SingleProductCard;
