import React from "react";
import PropTypes from "prop-types";

import "./Navbar.css";

const Navbar = ({ onHomePress, onCartPress, cartTotalProducts }) => {
  return (
    <nav>
      <div className="container">
        <div
          className="navbar__logo"
          onClick={onHomePress}
          style={{ cursor: "pointer" }}
        >
          ProductList
        </div>
        <ul className="navbar__navlist">
          <li className="navbar__navitem">
            <a className="navbar__navlink" onClick={onCartPress}>
              Cart [{cartTotalProducts}]
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
};

Navbar.propTypes = {
  onHomePress: PropTypes.func,
  onCartPress: PropTypes.func,
  cartTotalProducts: PropTypes.number,
};

export default Navbar;
