import React, { useRef } from "react";
import PropTypes from "prop-types";

import useFocusOnKeyDown from "react-focus-onkeydown";

import "./Search.css";

const Search = ({ setQuery }) => {
  const ref = useRef(null);
  useFocusOnKeyDown(ref);

  return (
    <div className="search">
      <form>
        <input
          type="text"
          ref={ref}
          onChange={(event) => {
            setTimeout(() => {
              setQuery(event.target.value);
            }, 2000);
          }}
          placeholder="Press any key to search"
        />
      </form>
    </div>
  );
};

Search.propTypes = {
  setQuery: PropTypes.func,
};

export default Search;
