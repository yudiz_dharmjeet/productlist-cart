import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";

import { BiDownArrow, BiUpArrow } from "react-icons/bi";

import "./CartItem.css";

const CartItem = ({
  cartItem,
  removeItem,
  cartTotalPrice,
  setCartTotalPrice,
}) => {
  const { title, image, price } = cartItem;

  const [cartItemQuantity, setCartItemQuantity] = useState(1);
  const [finalPrice, setFinalPrice] = useState(price);

  function cartItemPlus() {
    setCartItemQuantity(cartItemQuantity + 1);
    setCartTotalPrice(cartTotalPrice + price);
  }

  function cartItemMinus() {
    if (cartItemQuantity <= 1) {
      setCartItemQuantity(cartItemQuantity);
      setCartTotalPrice(cartTotalPrice);
    } else {
      setCartItemQuantity(cartItemQuantity - 1);
      setCartTotalPrice(cartTotalPrice - price);
    }
  }

  useEffect(() => {
    setFinalPrice(price * cartItemQuantity);
  }, [cartItemQuantity]);

  return (
    <div className="cartitem__div">
      <img src={image} alt={title} width={40} />
      <h4>{title}</h4>
      <div className="quantity">
        <BiUpArrow style={{ cursor: "pointer" }} onClick={cartItemPlus} />
        <h3>{cartItemQuantity}</h3>
        <BiDownArrow style={{ cursor: "pointer" }} onClick={cartItemMinus} />
      </div>
      <h3>${finalPrice.toFixed(2)}</h3>
      <button className="btn" onClick={() => removeItem(cartItem, finalPrice)}>
        Remove
      </button>
    </div>
  );
};

CartItem.propTypes = {
  cartItem: PropTypes.shape({
    title: PropTypes.string,
    image: PropTypes.string,
    price: PropTypes.number,
  }),
  removeItem: PropTypes.func,
  cartTotalPrice: PropTypes.number,
  setCartTotalPrice: PropTypes.func,
};

export default CartItem;
