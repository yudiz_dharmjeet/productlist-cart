import React, { useState } from "react";

import PropTypes from "prop-types";
import "./ProductList.css";

import SingleProductCard from "../SingleProductCard/SingleProductCard";
import Search from "../Search/Search";

const ProductList = ({
  addCartItem,
  removeItem,
  cartItems,
  products,
  isLoading,
  removeItemFromHome,
}) => {
  const [query, setQuery] = useState("");

  return (
    <>
      <div className="container productList__container">
        {isLoading ? (
          <h3 style={{ marginTop: "30px" }}>Loading...</h3>
        ) : (
          <div className="main_productlist_div">
            <Search setQuery={setQuery} />
            <div className="productList">
              {products
                .filter((singleProduct) => {
                  if (query === "") {
                    return singleProduct;
                  } else if (
                    singleProduct.title
                      .toLowerCase()
                      .includes(query.toLowerCase())
                  ) {
                    return singleProduct;
                  }
                })
                .map((singleProduct) => (
                  <SingleProductCard
                    key={singleProduct.id}
                    singleProduct={singleProduct}
                    addCartItem={addCartItem}
                    removeItem={removeItem}
                    cartItems={cartItems}
                    removeItemFromHome={removeItemFromHome}
                  />
                ))}
            </div>
          </div>
        )}
      </div>
    </>
  );
};

ProductList.propTypes = {
  addCartItem: PropTypes.func,
  removeItem: PropTypes.func,
  cartItems: PropTypes.array,
  products: PropTypes.array,
  isLoading: PropTypes.bool,
  removeItemFromHome: PropTypes.func,
};

export default ProductList;
