import React from "react";
import PropTypes from "prop-types";

import "./Cart.css";
import CartItem from "../CartItem/CartItem";

const Cart = ({
  cartItems,
  removeItem,
  clearCart,
  buyNow,
  cartTotalPrice,
  setCartTotalPrice,
}) => {
  return (
    <>
      <div
        className="container"
        style={{ marginTop: "20px", marginBottom: "20px" }}
      >
        <div className="cart__top">
          <h3>Total Products : {cartItems.length}</h3>
          <button className="btn" onClick={clearCart}>
            Clear Cart
          </button>
        </div>
        <div className="cart__middle">
          {cartItems.length == 0 ? (
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                margin: "30px",
              }}
            >
              <h1>Your cart is empty</h1>
            </div>
          ) : (
            <>
              {cartItems.map((cartItem) => (
                <CartItem
                  key={cartItem.id}
                  cartItem={cartItem}
                  removeItem={removeItem}
                  cartTotalPrice={cartTotalPrice}
                  setCartTotalPrice={setCartTotalPrice}
                />
              ))}
            </>
          )}
        </div>
        <div className="cart__bottom">
          <div></div>
          <div>
            <div className="cart__bottom__right">
              <h3>Total : ${cartTotalPrice.toFixed(2)}</h3>
              <button
                className="btn"
                style={{ marginTop: "20px" }}
                onClick={buyNow}
              >
                Buy Now
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

Cart.propTypes = {
  cartItems: PropTypes.array,
  removeItem: PropTypes.func,
  clearCart: PropTypes.func,
  buyNow: PropTypes.func,
  cartTotalPrice: PropTypes.number,
  setCartTotalPrice: PropTypes.func,
};

export default Cart;
